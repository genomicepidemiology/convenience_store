#!/usr/bin/env python3

from argparse import ArgumentParser
import xmltodict

from libs.tsv import TSV


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-i", "--input",
                        help=("Path to xml input file(s)."),
                        nargs="+",
                        required=True)
    parser.add_argument("-o", "--output",
                        help="Path to output file.",
                        required=True)

    args = parser.parse_args()

    tsv = TSV(args.input)

    header_order = ["sample acc", "sample alias"]
    print(tsv.to_string(header_order))

    # https://www.ebi.ac.uk/ena/browser/api/xml/ERS3335870-ERS3335875?download=true   PRJEB31846
    # https://www.ebi.ac.uk/ena/browser/api/xml/PRJEB31846?download=true
