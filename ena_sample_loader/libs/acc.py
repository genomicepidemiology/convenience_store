#!/usr/bin/env python3

from libs.xml.sample import Sample
from libs.xml.project import Project


class Acc():
    def __init__(self, xml_path):
        self.xml_path = xml_path
        self.type = self.get_type(xml_path)

        # Initiate sets for related accession numbers
        self.acc_submission = set()
        self.acc_project = set()
        self.acc_sample = set()
        self.acc_experiment = set()
        self.acc_run = set()
        # Load the initiated sets
        self._get_related_accs()

    @staticmethod
    def get_type(xml_path):
        type = None
        with open(xml_path, "r") as fh:
            fh.readline()
            set_start = fh.readline().rstrip()
        if(set_start == "<PROJECT_SET>"):
            type = "project"
        elif(set_start == "<SAMPLE_SET>"):
            type = "sample"
        elif(set_start == "<RUN_SET>"):
            type = "run"
        elif(set_start == "<SUBMISSION_SET>"):
            type = "submission"
        return type

    def _get_related_accs(self):
        if(self.type == "sample"):
            self._get_related_accs_to_sample()
        elif(self.type == "project"):
            self._get_related_accs_to_project()

    def _get_related_accs_to_sample(self):
        sample = Sample([self.xml_path])
        for row in sample:
            self.acc_submission.add(row["ena-submission"])
            self.acc_project.add(row["ena-study"])
            self.acc_sample.add(row["sample acc"])
            self.acc_experiment.add(row["ena-experiment"])
            self.acc_run.add(row["ena-run"])

    def _get_related_accs_to_project(self):
        project = Project([self.xml_path])
        print(project.to_string())
        for row in project:
            self.acc_submission.add(row["ena-submission"])
            self.acc_project.add(row["ena-project"])
            self.acc_sample.add(row["ena-sample"])
            self.acc_experiment.add(row["ena-experiment"])
            self.acc_run.add(row["ena-run"])

    def expand_accs():
        pass

    def compress_accs():
        pass
