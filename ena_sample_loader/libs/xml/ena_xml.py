#!/usr/bin/env python3

import xmltodict
import sys
import logging


class ENA_XML(list):

    REQ_ENTRY = {}
    REQUIRED_SPECIAL_ENTRY = ("IDENTIFIERS")

    def __init__(self, xml_paths, ena_set, sep="\t"):
        self.headers = set()
        self.sep = sep

        for path in xml_paths:
            with open(path) as fh:
                xmldict = xmltodict.parse(fh.read())
            self._parse_xmldict(xmldict, ena_set)

    def to_string(self, header_order=[]):
        header_order = self._create_header_order(header_order)
        out_list = [self.sep.join(header_order)]

        for row in self:
            row_list = []
            for header in header_order:
                val = row.get(header, "")
                row_list.append(val)
            row_str = self.sep.join(row_list)
            out_list.append(row_str)

        return "\n".join(out_list)

    def _create_header_order(self, given_header_order):
        diff_headers = self.headers.difference(set(given_header_order))
        miss_headers = set(given_header_order).difference(self.headers)

        if(miss_headers):
            logging.debug("One or more header(s) in the header order list was"
                          " not found. Missing header(s): {}. Available "
                          "headers: {}".format(miss_headers, self.headers))

        return list(given_header_order) + list(diff_headers)

    def _parse_set(self, set_in):
        """
            SAMPLE_SET is always a dict with one entry.
            The type of the entry changes depending if one or more
            samples are found in the sample set.
            >1 samples in set: List of sample dicts
            1 sample in set: sample dict
        """
        if(len(set_in.values()) > 1):
            sys.exit("ERROR in method parse_sample_set() in class TSV. "
                     "Assumption about only one entry in the sample_set dict "
                     "did not hold true.")

        for entry in set_in.values():
            set_out = entry
            break

        if(not isinstance(set_out, list)):
            set_out = [set_out]

        return set_out

    def _parse_xmldict(self, xmldict, set_id):
        ena_set = self._parse_set(xmldict[set_id])

        for entry in ena_set:
            row = {}
            self.append(row)
            self._parse_simple_entries(entry, row)
            self._parse_identifiers(entry, row)

    def _parse_simple_entries(self, entry, row):
        for xml_key, header in self.REQ_ENTRY.items():
            val = entry.get(xml_key, False)
            if(val):
                row[header] = entry[xml_key]
                self.headers.add(header)

    def _parse_identifiers(self, entry, row):
        identifier_xml = entry[self.REQUIRED_SPECIAL_ENTRY[0]]
        self._load_namespace_text_entry("EXTERNAL_ID", identifier_xml, row)
        self._load_simple_entry("SECONDARY_ID", identifier_xml, row)

    def _parse_links(self, entry, row, entry_label):
        xml = entry.get(entry_label, {})
        for i, link in enumerate(xml.values()):
            for xref_entry in link:
                if(i == 0):
                    i = ""
                xref = xref_entry.get("XREF_LINK", {})
                db = xref.get("DB", False)
                id = xref.get("ID", False)
                if(all([xref, db, id])):
                    name = "{}{}".format(db, i).lower()
                    row[name] = id
                    self.headers.add(name)

    def _parse_attributes(self, entry, row, entry_label):
        xml = entry.get(entry_label, {})
        for i, attr in enumerate(xml.values()):
            for attr_entry in attr:
                if(i == 0):
                    i = ""
                tag = attr_entry.get("TAG", False)
                val = attr_entry.get("VALUE", False)
                if(all([tag, val])):
                    name = "{}{}".format(tag, i).lower()
                    row[name] = val
                    self.headers.add(name)

    def _load_namespace_text_entry(self, label, xml, row):
        label_dict = xml.get(label, {})
        name = label_dict.get("@namespace", "").lower()
        text = label_dict.get("#text", "")
        if(all([name, text])):
            row[name] = text
            self.headers.add(name)

    def _load_simple_entry(self, label, xml, row):
        val = xml.get(label, False)
        if(val):
            header = label.lower()
            row[header] = val
            self.headers.add(header)

    def _load_dict_entry(self, entry, row, entry_label):
        xml = entry.get(entry_label, {})
        for header, val in xml.items():
            header = header.lower()
            self.headers.add(header)
            row[header] = val
