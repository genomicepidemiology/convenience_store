#!/usr/bin/env python3

import xmltodict
import sys
import logging


class Sample(list):

    REQUIRED_SIMPLE_ENTRY = {
        "@accession": "ena-sample",
        "@alias": "sample alias",
        "@center_name": "center",
        "TITLE": "project title",
        "DESCRIPTION": "project desciption"
    }

    REQUIRED_SPECIAL_ENTRY = (
        "IDENTIFIERS",
        "SAMPLE_NAME",
        "SAMPLE_LINKS",
        "SAMPLE_ATTRIBUTES"
    )

    def __init__(self, xml_paths, sep="\t"):
        self.headers = set()
        self.sep = sep

        for path in xml_paths:
            with open(path) as fh:
                xmldict = xmltodict.parse(fh.read())
            self.parse_xmldict(xmldict)

    def to_string(self, header_order=[]):
        header_order = self.create_header_order(header_order)
        out_list = [self.sep.join(header_order)]

        for row in self:
            row_list = []
            for header in header_order:
                val = row.get(header, "")
                row_list.append(val)
            row_str = self.sep.join(row_list)
            out_list.append(row_str)

        return "\n".join(out_list)

    def create_header_order(self, given_header_order):
        diff_headers = self.headers.difference(set(given_header_order))
        miss_headers = set(given_header_order).difference(self.headers)

        if(miss_headers):
            logging.debug("One or more header(s) in the header order list was"
                          " not found. Missing header(s): {}. Available "
                          "headers: {}".format(miss_headers, self.headers))

        return list(given_header_order) + list(diff_headers)

    def parse_sample_set(self, sample_set_in):
        """
            SAMPLE_SET is always a dict with one entry.
            The type of the entry changes depending if one or more
            samples are found in the sample set.
            >1 samples in set: List of sample dicts
            1 sample in set: sample dict
        """
        if(len(sample_set_in.values()) > 1):
            sys.exit("ERROR in method parse_sample_set() in class TSV. "
                     "Assumption about only one entry in the sample_set dict "
                     "did not hold true.")

        for entry in sample_set_in.values():
            sample_set_out = entry
            break

        if(not isinstance(sample_set_out, list)):
            sample_set_out = [sample_set_out]

        return sample_set_out

    def parse_xmldict(self, xmldict):
        sample_set = self.parse_sample_set(xmldict["SAMPLE_SET"])

        for sample in sample_set:
            row = {}
            self.append(row)
            self.parse_simple_entries(sample, row)
            self.parse_identifiers(sample, row)
            self.parse_sample_name(sample, row)
            self.parse_sample_links(sample, row)
            self.parse_sample_attributes(sample, row)

    def parse_simple_entries(self, sample, row):
        for xml_key, header in self.REQUIRED_SIMPLE_ENTRY.items():
            val = sample.get(xml_key, False)
            if(val):
                row[header] = sample[xml_key]
                self.headers.add(header)

    def parse_identifiers(self, sample, row):
        identifier_xml = sample[self.REQUIRED_SPECIAL_ENTRY[0]]
        ext_id = identifier_xml.get("EXTERNAL_ID", {})
        name = ext_id.get("@namespace").lower()
        id = ext_id.get("#text")
        if(all([name, id])):
            row[name] = id
            self.headers.add(name)

    def parse_sample_name(self, sample, row):
        sample_name_xml = sample[self.REQUIRED_SPECIAL_ENTRY[1]]
        for header, entry in sample_name_xml.items():
            header = header.lower()
            self.headers.add(header)
            row[header] = entry

    def parse_sample_links(self, sample, row):
        sample_links_xml = sample[self.REQUIRED_SPECIAL_ENTRY[2]]
        for i, link in enumerate(sample_links_xml.values()):
            for xref_entry in link:
                if(i == 0):
                    i = ""
                xref = xref_entry.get("XREF_LINK", {})
                db = xref.get("DB", False)
                id = xref.get("ID", False)
                if(all([xref, db, id])):
                    name = "{}{}".format(db, i).lower()
                    row[name] = id
                    self.headers.add(name)

    def parse_sample_attributes(self, sample, row):
        sample_attributes_xml = sample[self.REQUIRED_SPECIAL_ENTRY[3]]
        for i, attr in enumerate(sample_attributes_xml.values()):
            for attr_entry in attr:
                if(i == 0):
                    i = ""
                tag = attr_entry.get("TAG", False)
                val = attr_entry.get("VALUE", False)
                if(all([tag, val])):
                    name = "{}{}".format(tag, i).lower()
                    row[name] = val
                    self.headers.add(name)
