#!/usr/bin/env python3

import xmltodict
import sys
import logging

from libs.xml.ena_xml import ENA_XML


class Project(ENA_XML):

    REQ_ENTRY = {
        "@accession": "ena-project",
        "@center_name": "center"
    }

    PRJ_ENTRIES = (
        "NAME",
        "TITLE",
        "DESCRIPTION"
    )

    REQUIRED_SPECIAL_ENTRY = (
        "IDENTIFIERS",
        "PROJECT_LINKS",
        "PROJECT_ATTRIBUTES"
    )

    def __init__(self, xml_paths, sep="\t"):
        ENA_XML.__init__(self, xml_paths, "PROJECT_SET", sep)

    def _parse_xmldict(self, xmldict, set_id):
        ena_set = self._parse_set(xmldict[set_id])

        for entry in ena_set:
            row = {}
            self.append(row)
            self._parse_simple_entries(entry, row)
            self._load_simple_entry(self.PRJ_ENTRIES[0], entry, row)
            self._load_simple_entry(self.PRJ_ENTRIES[1], entry, row)
            self._load_simple_entry(self.PRJ_ENTRIES[2], entry, row)
            self._parse_identifiers(entry, row)
            self._parse_links(entry, row, self.REQUIRED_SPECIAL_ENTRY[1])
            self._parse_attributes(entry, row, self.REQUIRED_SPECIAL_ENTRY[2])
