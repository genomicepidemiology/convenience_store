#!/usr/bin/env python3

import os
import subprocess
from argparse import ArgumentParser
import xmltodict

from libs.acc import Acc


def download_xmls(accs, dir):
    cmd_download = ["wget", "-q", "-O"]
    for acc in accs:
        url = ("https://www.ebi.ac.uk/ena/browser/api/xml/{}?download=true"
               .format(acc))
        out_dest = "{}/{}.xml".format(dir, acc)
        subprocess.run(cmd_download + [out_dest, url], check=True)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-i", "--input",
                        help=("ENA accession numbers."),
                        nargs="+",
                        required=True)
    parser.add_argument("-d", "--data",
                        help="Path to data folder.",
                        required=True)
    parser.add_argument("-o", "--output",
                        help="Path to output file.")

    args = parser.parse_args()

    args.data = os.path.abspath(args.data)
    os.makedirs(args.data, exist_ok=True)

    tmp_dir = ("{}/tmp_xml".format(args.data))
    os.makedirs(tmp_dir, exist_ok=True)

    download_xmls(args.input, tmp_dir)

    dir_submission_xml = "{}/xml/submission".format(args.data)
    dir_project_xml = "{}/xml/project".format(args.data)
    dir_sample_xml = "{}/xml/sample".format(args.data)
    dir_experiment_xml = "{}/xml/experiment".format(args.data)
    dir_run_xml = "{}/xml/run".format(args.data)
    os.makedirs(dir_submission_xml, exist_ok=True)
    os.makedirs(dir_project_xml, exist_ok=True)
    os.makedirs(dir_sample_xml, exist_ok=True)
    os.makedirs(dir_experiment_xml, exist_ok=True)
    os.makedirs(dir_run_xml, exist_ok=True)

    for acc in args.input:
        xml_path = "{}/{}.xml".format(tmp_dir, acc)
        acc = Acc(xml_path)
        download_xmls(acc.acc_submission, dir_submission_xml)
        download_xmls(acc.acc_project, dir_project_xml)
        download_xmls(acc.acc_sample, dir_sample_xml)
        download_xmls(acc.acc_experiment, dir_experiment_xml)
        download_xmls(acc.acc_run, dir_run_xml)
